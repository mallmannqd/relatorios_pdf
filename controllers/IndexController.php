<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 08/04/18
 * Time: 16:50
 */

namespace controllers;

use core\Controller;
use Mpdf\Mpdf;

class IndexController extends Controller
{
    public function index()
    {
        ob_start();
        ?>

        <h1>Olá Mundo</h1><h4>Sub título etc...</h4>
        <?php

        $html = ob_get_contents();
        ob_end_clean();

        $mpdf = new Mpdf();
        $mpdf->WriteHTML($html);


        // Sem argumentos ele imprimi o pdf no navegador
        // Para fazer o download do arquivo, o primeiro argumento deve ser o nome do arquivo e o segundo a ação a ser feita
        // Ação I = Abra no browser
        // Ação D = Faça o Download
        // Ação F = salva no servidor

        $mpdf->Output('arquivo.pdf', 'I');
    }

}