<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 08/04/18
 * Time: 11:46
 */
use \core\Core as Core;

session_start();
require_once 'env.php';
require_once 'autoloader.php';
require_once 'vendor/autoload.php';

$core = new Core();
$core->run();